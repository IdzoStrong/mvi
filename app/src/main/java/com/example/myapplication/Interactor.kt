package com.example.myapplication

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class NotificationInteractor {

    fun fetch(){}

    fun observe(): Flow<List<Notification>> {
        return flow {
            emit(listOf(
                Notification(),
                Notification(),
                Notification()
            ))
        }
    }
}

class Notification