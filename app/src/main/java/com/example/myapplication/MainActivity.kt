package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe

class MainActivity : AppCompatActivity() {

    val vm get() = ViewModelProviders.of(this).get(Feature::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        vm.state.observe(this){

        }
    }
}