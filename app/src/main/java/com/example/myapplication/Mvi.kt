package com.example.myapplication

import androidx.lifecycle.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*


abstract class Feature<A, S, N>: ViewModel() {
    protected abstract val store: Store<A, S, N>

    val state = MutableLiveData<S>()
    val news = SingleLiveEvent<N>()

    init {
        store.state
            .onEach { state.value = it }
            .launchIn(viewModelScope)

        store.news
            .onEach { news.value = it }
            .launchIn(viewModelScope)

        store.wire(viewModelScope)
    }

    fun add(action: A){
        store.add(action)
    }
}

typealias Reducer<S, A> = (state: S, action: A) -> S
typealias Middleware<A, S, Output> = (actions: Flow<A>, states: Flow<S>) -> Flow<Output>

class Store<A, S, N>(
    private val initialState: S,
    private val reducer: Reducer<S, A>,
    private val middlewares: List<Middleware<A,S,A>> = emptyList(),
    private val newsHandler: List<Middleware<A,S,N>> = emptyList()
) {
    private val _state = BroadcastChannel<S>(capacity = Channel.BUFFERED).apply {
        offer(initialState)
    }
    private val _newsState = BroadcastChannel<N>(capacity = Channel.BUFFERED)
    private val _actions = BroadcastChannel<A>(capacity = Channel.BUFFERED)

    val state = _state.asFlow()
    val news = _newsState.asFlow()

    fun wire(scope: CoroutineScope) {
        val actions = _actions.asFlow()

        actions
            .withLatestFrom(state) { a: A, s: S ->
                reducer(s, a)
            }
            .distinctUntilChanged()
            .onEach { _state.send(it) }
            .launchIn(scope)


        middlewares.map { it(actions, state) }
            .merge()
            .onEach { _actions.send(it) }
            .launchIn(scope)

        newsHandler.map { it(actions, state) }
            .merge()
            .onEach { _newsState.send(it) }
            .launchIn(scope)
    }

    fun add(action: A){
        _actions.offer(action)
    }
}
