package com.example.myapplication

import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach


class NotificationsFeature: Feature<NotificationsFeature.Action, NotificationsFeature.State, NotificationsFeature.News>() {
    val interactor = NotificationInteractor()

    val reducer = { state: State, action: Action ->
        when (action){
            Action.OnInit -> state.copy(
                progress = true
            )
            else -> state
        }
    }

    val middlewares = listOf<Middleware<Action, State, Action>>(
        { actions, states ->
            actions.typeOf(Action.OnInit::class)
                .flatMapLatest { interactor.observe() }
                .map { Action.OnChangedData(it) }
        },
        { actions, states ->
            actions.typeOf(Action.OnFetch::class)
                .onEach { interactor.fetch() }
                .filter { false }
        }
    )

    val newsHandler = listOf<Middleware<Action, State, News>>(
        { actions, states ->
            actions.typeOf(Action.OnClick::class)
                .map { News.OpenDetails(it.categoryId) }
        }
    )

    override val store = Store(
        initialState = State(
            notifications = emptyList(),
            progress = true
        ),
        reducer = reducer,
        middlewares = middlewares,
        newsHandler = newsHandler
    )

    init {
        add(Action.OnInit)
    }

    data class State(
        val notifications: List<Any>,
        val progress: Boolean
    )

    sealed class Action {
        object OnInit: Action()
        class OnClick(val categoryId: Long): Action()
        object OnFetch: Action()
        class OnChangedData(val notifications: List<Notification>): Action()
    }
    sealed class News {
        class OpenDetails(val id: Long): News()
    }
}

val Any?.safe get() = Unit