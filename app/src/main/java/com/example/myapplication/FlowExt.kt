package com.example.myapplication

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.cancel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.reflect.KClass

private object UNINITIALIZED

@JvmOverloads
fun <T> Flow<T>.asSingleEventLiveData(
    context: CoroutineContext = EmptyCoroutineContext,
    timeoutInMs: Long
): LiveData<T> = liveData(context, timeoutInMs) {
    var lastItem: T?
    val pending = AtomicBoolean(false)

    collect {
        if (pending.compareAndSet(true, false)){

        }

        lastItem = it

        emit(it)
    }
}

inline fun <A, reified B: Any> Flow<A>.typeOf(cl: KClass<B>): Flow<B> {
    return filter { it is B }
        .map { it as B }
}

fun <A, B, R> Flow<A>.withLatestFrom(other: Flow<B>, transform: suspend (A, B) -> R): Flow<R> {
    return flow {
        coroutineScope {
            val latestB = AtomicReference<Any>(UNINITIALIZED)
            val outerScope = this

            launch {
                try {
                    other.collect { latestB.set(it) }
                } catch (e: CancellationException) {
                    outerScope.cancel(e) // cancel outer scope on cancellation exception, too
                }
            }

            collect { a ->
                val b = latestB.get()
                if (b != UNINITIALIZED) {
                    @Suppress("UNCHECKED_CAST")
                    emit(transform(a, b as B))
                }
            }
        }
    }
}